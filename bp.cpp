/* 046267 Computer Architecture - Winter 2019/20 - HW #1 */
/* This file should hold your implementation of the predictor simulator */

#include "bp_api.h"
#include <vector>
#include "stdlib.h"
#include "math.h"

#define SIZE_OF_BTB_ROW 3
#define SIZE_OF_WORD 32
#define TAG_INDEX 0

#define TARGET_SIZE 30

#define NO_SHARE 0
#define LSB_SHARE 1
#define MID_SHARE 2

uint32_t create_mask(unsigned mask_size);
unsigned get_total_size_in_bits();
void update_history(uint32_t* hist_ptr, bool taken);

class FSM {

    public:

    int cnt;
    unsigned initial_state;

    FSM(unsigned initial_state): cnt(initial_state), initial_state(initial_state){};

    void inc_state(){ if (cnt < 3) cnt++; }
    void dec_state(){ if (cnt > 0) cnt--; }

    void update_fsm(bool taken){
        if(taken) inc_state();
        else dec_state();
    }

    void reset_fsm(){
        cnt = initial_state;
    }

    bool get_prediction() {
        if (cnt == 2 || cnt == 3) {
            return true;
        } else {
            return false;
        }
    }

};

class BTB_Entry {
    public:

    uint32_t tag;
    uint32_t dest;
    uint32_t* hist_ptr;
    FSM** fsm_array_ptr;

    BTB_Entry(uint32_t* hist_ptr, FSM** fsm_array_ptr) {
        tag = 0;
        dest = 0;
        this->hist_ptr = hist_ptr;
        this->fsm_array_ptr = fsm_array_ptr;
    }
};

class BTB {
public:
    unsigned btb_size;
    unsigned history_size;
    unsigned tag_size;
    unsigned fsm_state;
    bool is_global_hist;
    bool is_global_table;
    int shared;
    std::vector<BTB_Entry*> table;
    SIM_stats* sim_stats;

    BTB(unsigned btb_size, unsigned history_size, unsigned tag_size,
        unsigned fsm_state, \
        bool is_global_hist, bool is_global_table, int shared) : \
        btb_size(btb_size), history_size(history_size), tag_size(tag_size), \
        fsm_state(fsm_state), is_global_hist(is_global_hist), \
        is_global_table(is_global_table), shared(shared), table(btb_size) {

        int num_of_fsm_array_entries = pow(2,history_size);

        if(is_global_hist == false) {
            if (is_global_table == false) {

            // private history and private fsm table for each
                for(unsigned i=0; i<btb_size; i++){

                    // alloc hist ptr
                    uint32_t* new_hist_ptr = new uint32_t(0);

                    // alloc fsm array and the fsms inside it
                    FSM** new_fsm_array = new FSM*[num_of_fsm_array_entries];
                    for(int j=0; j<num_of_fsm_array_entries; j++) {
                        new_fsm_array[j] = new FSM(fsm_state);
                    }

                    table[i] = new BTB_Entry(new_hist_ptr, new_fsm_array);
                }
            } else {

            // private history and global fsm table for each

                // alloc fsm array and the fsms inside it
                FSM** new_fsm_array = new FSM*[num_of_fsm_array_entries];
                for(int j=0; j<num_of_fsm_array_entries; j++) {
                    new_fsm_array[j] = new FSM(fsm_state);
                }

                for(unsigned i=0; i<btb_size; i++){

                    // alloc hist ptr
                    uint32_t* new_hist_ptr = new uint32_t(0);

                    table[i] = new BTB_Entry(new_hist_ptr, new_fsm_array);
                }
            }
        } else {
            if (is_global_table == false) {

            // global history and private fsm table for each
                uint32_t* new_hist_ptr = new uint32_t(0);

                for(unsigned i=0; i<btb_size; i++){

                    // alloc fsm array and the fsms inside it
                    FSM** new_fsm_array = new FSM*[num_of_fsm_array_entries];
                    for(int j=0; j<num_of_fsm_array_entries; j++) {
                        new_fsm_array[j] = new FSM(fsm_state);
                    }

                    table[i] = new BTB_Entry(new_hist_ptr, new_fsm_array);
                }
            } else {
            // global history and global fsm table for each

                uint32_t* new_hist_ptr = new uint32_t(0);

                // alloc fsm array and the fsms inside it
                FSM** new_fsm_array = new FSM*[num_of_fsm_array_entries];
                for(int j=0; j<num_of_fsm_array_entries; j++) {
                    new_fsm_array[j] = new FSM(fsm_state);
                }

                for(unsigned i=0; i<btb_size; i++) {
                    table[i] = new BTB_Entry(new_hist_ptr, new_fsm_array);
                }
            }
        }

    // init sim_stats:
        sim_stats = new SIM_stats{0,0,0};


    }

    ~BTB(){

        int num_of_fsm_array_entries = pow(2,history_size);

        if(is_global_hist == false) {
            if (is_global_table == false) {

                // private history and private fsm table for each
                for(unsigned i=0; i<btb_size; i++){

                    delete table[i]->hist_ptr;

                    // delete all fsms in this entry's fsm_array
                    for(int j=0; j<num_of_fsm_array_entries; j++) {
                        delete table[i]->fsm_array_ptr[j];
                    }

                    // delete fsm array of this entry
                    delete table[i]->fsm_array_ptr;

                }
            } else {

            // private history and global fsm table for each

                // delete all fsms in the global fsm_array
                for(int j=0; j<num_of_fsm_array_entries; j++) {
                    delete table[0]->fsm_array_ptr[j];
                }

                // delete global fsm array
                delete table[0]->fsm_array_ptr;

                for(unsigned i=0; i<btb_size; i++){
                    delete table[i]->hist_ptr;

                }
            }

        } else {
            if (is_global_table == false) {

                // global history and private fsm table for each

                delete table[0]->hist_ptr; // delete global hist_ptr

                for(unsigned i=0; i<btb_size; i++){

                    // delete all fsms in this entry's fsm_array
                    for(int j=0; j<num_of_fsm_array_entries; j++) {
                        delete table[i]->fsm_array_ptr[j];
                    }

                    // delete fsm array of this entry
                    delete table[i]->fsm_array_ptr;

                }

            } else {
                // global history and global fsm table for each

                // delete all fsms in the global fsm_array
                for(int j=0; j<num_of_fsm_array_entries; j++) {
                    delete table[0]->fsm_array_ptr[j];
                }

                delete table[0]->fsm_array_ptr; // delete global fsm array
                delete table[0]->hist_ptr; // delete global hist_ptr
            }
        }

        for(unsigned i=0; i<btb_size; i++){
            delete table[i];
        }

        // delete sim_stats
            delete sim_stats;
    }
};

BTB* btb;

int BP_init(unsigned btbSize, unsigned historySize, unsigned tagSize, unsigned fsmState,
			bool isGlobalHist, bool isGlobalTable, int Shared){

    btb = new BTB(btbSize, historySize, tagSize, fsmState, isGlobalHist, \
                  isGlobalTable, Shared);

    btb->sim_stats->size = get_total_size_in_bits(); // calculate total size
    if (btb == nullptr) {
        return -1;
    } else {
        return 0;
    }

}

/*
 * returns prediction for the given pc [as in IF stage].
 * true = Taken
 * false = Not Taken
 */
bool BP_predict(uint32_t pc, uint32_t *dst){

    // check if tag is in table
    // calculate btb entry
    uint32_t btb_entry = pc;
    btb_entry = btb_entry >> 2;
    btb_entry = btb_entry & create_mask(unsigned(log2(btb->btb_size)));  // only possible values are powers of 2
                                                                   // log of btb_size is the number of entries in the table.

    uint32_t pc_to_tag = pc;
    pc_to_tag = pc_to_tag >> 2;
    if (btb->table[btb_entry]->tag == (pc_to_tag & create_mask(btb->tag_size))) {  // check if tag is in table

        // tag is in the btb, get taken/not taken for this pc and return prediction
        uint32_t fsm_table_entry = get_fsm_entry(*(btb->table[btb_entry]->hist_ptr), btb->history_size, pc, btb->shared);
        bool is_taken = btb->table[btb_entry]->fsm_array_ptr[fsm_table_entry]->get_prediction();

        if (is_taken) {
            *dst = uint32_t(btb->table[btb_entry]->dest);
            return true;
        }
        else {
            *dst = pc+4;
            return false;
        }

    } else {  // tag not in the btb - predict NT
        *dst = pc+4;
        return false;
    }

}

void BP_update(uint32_t pc, uint32_t targetPc, bool taken, uint32_t pred_dst){

    int num_of_fsm_array_entries = int(pow(2,btb->history_size));

    bool is_flush = false;

    // update branch num
    btb->sim_stats->br_num++;

    // calculate btb entry
    uint32_t btb_entry = pc;
    btb_entry = btb_entry >> 2;
    btb_entry = btb_entry & create_mask(unsigned(log2(btb->btb_size)));  // only possible values are powers of 2
    // log of btb_size is the number of entries in the table.

    uint32_t pc_to_tag = pc;
    pc_to_tag = pc_to_tag >> 2;
    uint32_t tag_from_pc = pc_to_tag & create_mask(btb->tag_size);

    // go into the fsm and check prediction:

    // check if the tag is already in the table
    if(btb->table[btb_entry]->tag == tag_from_pc){

        // tag is in the btb, get prediction for this pc
        uint32_t fsm_table_entry = get_fsm_entry(*(btb->table[btb_entry]->hist_ptr), btb->history_size, pc, btb->shared);
        bool pred = btb->table[btb_entry]->fsm_array_ptr[fsm_table_entry]->get_prediction();

        if(pred != taken){
            //we got the wrong prediciton
            is_flush = true;
        }
        // update dest if needed
        if(targetPc != pred_dst){ //FIXME - check what to do in this case
            if(taken) is_flush = true; // flush if we took the branch but with the wrong target
            btb->table[btb_entry]->dest = targetPc;
        }

        // update fsm - global / local - doesn't matter
        btb->table[btb_entry]->fsm_array_ptr[fsm_table_entry]->update_fsm(taken);

        // update history - global / local - doesn't matter
        update_history(btb->table[btb_entry]->hist_ptr,taken);


    } else{  // tag is not in the table, so we predicted NT

        if(taken) { // we mispredicted
            is_flush = true;
        }

        // insert tag to the btb table:
        btb->table[btb_entry]->tag = tag_from_pc;
        btb->table[btb_entry]->dest = targetPc;

        if(btb->is_global_table == false){ // for local fsm array , we reset it for the new tag
            for(int i=0; i < num_of_fsm_array_entries; i++){
                btb->table[btb_entry]->fsm_array_ptr[i]->reset_fsm();
            }
            //FIXME: new addition - need to validate it
            uint32_t fsm_table_entry = get_fsm_entry(*(btb->table[btb_entry]->hist_ptr), btb->history_size, pc, btb->shared);
            btb->table[btb_entry]->fsm_array_ptr[fsm_table_entry]->update_fsm(taken);

        } else{
             // for global fsm array , we just update
            uint32_t fsm_table_entry = get_fsm_entry(*(btb->table[btb_entry]->hist_ptr), btb->history_size, pc, btb->shared);
            btb->table[btb_entry]->fsm_array_ptr[fsm_table_entry]->update_fsm(taken);
        }
        if(btb->is_global_hist == false){ // for local history, we reset the history for the new tag
            *btb->table[btb_entry]->hist_ptr = uint32_t(taken); // if taken=true => history = 1 , else => history = 0
        } else{ //for global history, we just update
            update_history(btb->table[btb_entry]->hist_ptr,taken);
        }

    }

    // inc flush num if needed:
    if(is_flush) btb->sim_stats->flush_num++;

}
// function updates the input sim_states to be the BTB sim_states that we calculated
void BP_GetStats(SIM_stats *curStats){

    curStats->flush_num = btb->sim_stats->flush_num;
    curStats->br_num = btb->sim_stats->br_num;
    curStats->size = btb->sim_stats->size;

	return;
}


uint32_t create_mask(unsigned mask_size) {
    return uint32_t(pow(2,mask_size)-1);
}

/*
 * shared - 0/1/2 - sharing method
 * returns entry for the fsm state array
 */
uint32_t get_fsm_entry(uint32_t history, int history_size, uint32_t pc, int shared) {
    switch (shared) {
        case(NO_SHARE): {
            return history;
        }
        case(LSB_SHARE): {
            pc = pc >> 2;
            pc = pc & create_mask(history_size);
            return pc ^ history;
        }
        case(MID_SHARE): {
            pc = pc >> 16;
            pc = pc & create_mask(history_size);
            return pc ^ history;
        }
    }
}

// function calculates and returns the total (theoretical) size of the predictor - in bits.
unsigned get_total_size_in_bits(){

    if(btb->is_global_hist == false){
        if(btb->is_global_table == false){
            // private history and private fsm array
            return btb->btb_size*(btb->tag_size + btb->history_size + TARGET_SIZE + 2*int(pow(2,btb->history_size)));

        } else{
            // private history and global fsm array
            return btb->btb_size*(btb->tag_size + btb->history_size + TARGET_SIZE) + 2*int(pow(2,btb->history_size));
        }

    } else{
        if(btb->is_global_table == false){
            // global history and private fsm array
            return btb->btb_size*(btb->tag_size + TARGET_SIZE + 2*int(pow(2,btb->history_size))) + btb->history_size;
        } else{
            // global history and global fsm array
            return btb->btb_size*(btb->tag_size + TARGET_SIZE ) + btb->history_size + 2*int(pow(2,btb->history_size));
        }

    }

}
// function updates history according to last resolution (T/NT)
void update_history(uint32_t* hist_ptr, bool taken){

    *hist_ptr = *hist_ptr << 1; //shift left
    *hist_ptr = *hist_ptr % int(pow(2,btb->history_size)); // get rid of MSBs
    *hist_ptr += int(taken); // update with taken

}